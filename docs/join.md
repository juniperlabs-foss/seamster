# seamster.join

## Join
```python
Join(self, join_sides: Tuple[seamster.join_side.JoinSide, seamster.join_side.JoinSide], vectorizer: sklearn.feature_extraction.text.TfidfVectorizer = None)
```

Base Class for the join of two joinsides

Args:
    join_sides (Tuple[JoinSide, JoinSide]): A tuple of JoinSide objects on which join operations are performed
    vectorizer (TfidfVectorizer): vectorizer to create

### tfidf_from_join
```python
Join.tfidf_from_join(self, n_char=3, **kwargs) -> None
```

given class attribute join_sides, create a tfidf transformation of entity names of both sides to tfidf matrixes

given two join sides create a tfidf vectorizer and transform the entity names of both sides to tfidf matrixes.
If js_a/js_b already has a vectorizer it will use this to transform entity name instead.

Args:
    n_char (int): number of characters with which to tokenize inputs
    **kwargs: additional args to be passed to TfidfVectorizer object instantiation

Returns:
    None


### match_matrix_from_joinsides
```python
Join.match_matrix_from_joinsides(self, ntop: int = 1, lower_bound: float = 0.8, use_threads: bool = True, n_jobs: int = 2, n_char: int = 3, **kwargs) -> None
```

given class attribute join_sides, derives a sparse array of cosine similarity calculations

Args:
    ntop (int): maximum number of matches to be returned
    lower_bound (float): lower bound of cosine similarity score to be used
    use_threads (bool): boolean to use multithread
    n_jobs (int): number of threads
    n_char (int): number of characters with which to tokenize inputs
    **kwargs: additional args to be passed to TfidfVectorizer object instantiation

Returns:
    None


### get_matches_df
```python
Join.get_matches_df(self, precision: int = 5, **kwargs) -> pandas.core.frame.DataFrame
```

Creates a joined data frame of the two joinside dataframes

Args:
    precision (int): number of digits of precision to return for similarity
    **kwargs: additional args to be passed to the `match_matrix_from_joinsides` method

Returns:
    pd.DataFrame: combined dataframe


## BasicJoin
```python
BasicJoin(self, join_sides: Tuple[seamster.join_side.JoinSide, seamster.join_side.JoinSide], vectorizer: sklearn.feature_extraction.text.TfidfVectorizer = None)
```

Join class that provides simple match on entity name

## NameZipJoin
```python
NameZipJoin(self, join_sides: Tuple[seamster.join_side.JoinSide, seamster.join_side.JoinSide], vectorizer: sklearn.feature_extraction.text.TfidfVectorizer = None)
```

Join class that matches on entity name and zip code

## NameZipEntTypeJoin
```python
NameZipEntTypeJoin(self, join_sides: Tuple[seamster.join_side.JoinSide, seamster.join_side.JoinSide], vectorizer: sklearn.feature_extraction.text.TfidfVectorizer = None)
```

Join Class that matches on zip code, entity type, and entity name

