# seamster.join_side

## JoinSide
```python
JoinSide(self, source: str, data: pandas.core.frame.DataFrame, entity_name_field: str, id_field: str, deepcopy=True, vectorizer: sklearn.feature_extraction.text.TfidfVectorizer = None, replacement_strings: List[Tuple] = None, entity_type_mappings: List[Tuple] = None, **kwargs)
```

Basic Class for joining of two Pandas Dataframes of business entities.

Args:
        source (str): name of datasource
        data (pd.DataFrame): dataframe of data to be joined
        entity_name_field (str): name of field that contains the entity name
        id_field (str): name of the field that contains the id
        deepcopy (bool): whether to make a deep copy of the data (otherwise will mutate orig df) default=True
        vectorizer (TfidfVectorizer):
        replacement_strings (List[Tuple]): list of replacement string tuples (e.g. ``(r"consult(ing|ant(s)?|ancy)", "consulting")``)
        entity_type_mappings (List[Tuple]): list of entity type mapping tuples (e.g. ``("LIMITED LIABILITY COMPANY", "llc")``)
        **kwargs: additional attributes to be appended to the class

