"""High Performance Fuzzy Business Entity Matching"""
VERSION = (0, 0, 1)

__title__ = "seamster"
__description__ = "High Performance Fuzzy Business Entity Matching"
__url__ = "http://gitlab.com/juniperlabs-foss/seamster"
__download_url__ = (
    "https://gitlab.com/juniperlabs-foss/seamster/repository/archive.tar.gz?ref=master"
)
__version__ = ".".join(map(str, VERSION))
__author__ = "Juniper Labs"
__author_email__ = "support@juniperlabs.io"
__license__ = "Apache 2.0"
__copyright__ = "Copyright 2019 Juniper Labs"

